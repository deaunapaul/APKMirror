An unofficial APKMirror client/web app (forked from the original, which is now abandoned).


Longer Description

An Android app that utilizes a WebView to browse APKMirror.
APKMirror provides APKs, as the name quite obviously suggests.
This app saves you the trouble of having to open up a browser and visit APKMirror by typing the URL,
and is the sole purpose of this app existing (because who uses stupid boring browsers
to browse sites when you can create an entire app for a site, amirite?).


Features

- Quick loading (depends on phone tbh; I've found that newer models load much faster)
- Choose any download manager (even a browser counts, ironically)
- Clean material design (I think)
- Small-ish APK size (1.6 MB; takes around 5-6 MB when installed on a phone (not including data and cache))
- (Unrelated to the functions of the app itself, but (I guess) technically counts as a feature)
If your phone is one of those models (like mine, currently) that don't prompt you to sign in when
connecting to a WiFi that requires signing in through a portal, and therefore have no clue what
site to visit to actually be able to sign in, this app will inadvertently let you open it in an
external browser (provided you are connected to the WiFi, but haven't signed in).


Things that constitute as Anti-Features (that aren't already shown by F-Droid)

- The app itself does NOT contain any ad libraries whatsoever (it is completely FOSS).
However, as anyone who has visited the APKMirror site probably knows, they do display ads.
As this app utilizes WebView, the site's ads will also end up being displayed in the app
(remember that they show ads to be able to keep their site up, so try not to think too harshly
of them if you're a privacy paranoiac).
